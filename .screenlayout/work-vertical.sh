#!/bin/sh
# where hidpi monitor eDP1 is AxB and normal one is BxC and scaling by ExF
#xrandr --output eDP1 --auto --output HDMI1 --auto --panning [C*E]x[D*F]+[A]+0 --scale [E]x[F] --right-of eDP1
# A = 2560
# C = 1920
# D = 1080
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.8x1.8 --panning 3456x1944+2560+0 --right-of eDP1
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.2x1.2 --panning 2304x1296+2560+0 --right-of eDP1
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.5x1.5 --panning 2880x1620+2560+0 --right-of eDP1
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.75x1.75 --panning 3360x1890+2560+0 --right-of eDP1
# N.B pos of 2561 because adding one pixel seems to work around cursor not reaching the side of the screens
# this way you also don't need panning!
xrandr --output eDP1 --auto --output HDMI2 --auto --pos 2561x0 --scale 1.6x1.6 --rotate left #--scale 1.6x1.6 --rotate left --panning 1728x3072+2560+1728

$HOME/.screenlayout/wallpaper.sh
