#!/bin/sh
xrandr --output eDP-1 --primary --mode 3200x1800 --pos 0x0 --rotate normal --output DP-1 --mode 4096x2304 --pos 3200x0 --rotate normal --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --off
$HOME/.screenlayout/wallpaper.sh
xinput --map-to-output 10 eDP-1
