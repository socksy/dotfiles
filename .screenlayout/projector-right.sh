#!/bin/sh
# where hidpi monitor eDP1 is AxB and normal one is BxC and scaling by ExF
#xrandr --output eDP1 --auto --output HDMI1 --auto --panning [C*E]x[D*F]+[A]+0 --scale [E]x[F] --right-of eDP1
# A = 2560
# C = 1280
# D = 720
#xrandr --output eDP1 --auto --output HDMI2 --auto --right-of eDP1 --panning 0x0+0+0 # --scale 1.6x1.6 --panning 2048x1152+2560+0 
xrandr --output eDP-1 --auto --output DP-1 --auto --right-of eDP-1 --panning 0x0+0+0 --set "Broadcast RGB" "Limited" # --scale 1.6x1.6 --panning 2048x1152+2560+0 
$HOME/.screenlayout/wallpaper.sh
