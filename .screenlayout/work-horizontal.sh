#!/bin/sh
# where hidpi monitor eDP1 is AxB and normal one is CxD and scaling by ExF
#xrandr --output eDP1 --auto --output HDMI1 --auto --panning [C*E]x[D*F]+[A]+0 --scale [E]x[F] --right-of eDP1
# A = 2560
# C = 1920
# D = 1080
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.8x1.8 --panning 3456x1944+2560+0 --right-of eDP1
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.2x1.2 --panning 2304x1296+2560+0 --right-of eDP1
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.5x1.5 --panning 2880x1620+2560+0 --right-of eDP1
#xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.75x1.75 --panning 3360x1890+2560+0 --right-of eDP1
xrandr --output eDP1 --auto --output HDMI2 --auto --scale 1.6x1.6 --panning 3072x1728+2560+0 --right-of eDP1 --rotate normal --output DP1 --auto --scale 1.6x1.6 --panning 3072x1728+2560+0 --right-of eDP1 --rotate normal --output DP2 --auto --scale 1.6x1.6 --panning 3072x1728+2560+0 --right-of eDP1 --rotate normal


$HOME/bin/wallpaper-horizontal

