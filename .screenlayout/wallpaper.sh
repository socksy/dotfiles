#!/bin/sh
WALLPAPER=`find ~/wallpapers/ | sort -R | tail -n 1`
/run/current-system/sw/bin/feh --bg-fill $WALLPAPER
