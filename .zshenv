#!/bin/sh
# `npm install -g` path is set with npm config get/set prefix
export PATH=$HOME/bin:$HOME/.npm-packages/bin:$PATH:$HOME/go/bin:$HOME/.emacs.d/bin
export GOPATH=$HOME/go
