;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here
(load! "+bindings.el")
(load! "+org.el")

(setq doom-theme 'doom-gruvbox)
(setq display-line-numbers-type nil)

;; ??????? can't run projectile file finding without it????
(setq browse-url-mosaic-program "firefox")

;; Trying helm again..
;; add recentf and bookmarks to switch buffers command
(setq ivy-use-virtual-buffers t)
(setq +ivy-project-search-engines '(rg ag))
(setq counsel-projectile-sort-files t)
(setq counsel-projectile-sort-buffers t)
(setq neo-buffer-width 16)
(setq neo-smart-open t)
(setq lsp-auto-guess-root nil)
(setq lsp-ui-sideline-show-code-actions nil)

(setq flyspell-lazy-idle-second 5)

(after! ivy
;; add recentf and bookmarks to switch buffers command
  (setq ivy-use-virtual-buffers t
        ivy-count-format ""))

(after! 'emojify (setq emojify-emoji-set "twemoji-v2-22"))

(defun +personal-org-use-packages ()
    (use-package! org-super-agenda
      :after org-agenda
      :init nil)

  (use-package! org-alert
    :ensure t
                                        ;:defer 20
    :config
    (setq org-alert-interval 600)
    (org-alert-enable)))

;(add-hook 'org-mode-hook #'+personal-org-use-packages)

(add-hook 'after-init-hook #'global-emojify-mode)
(add-hook 'clojure-mode-hook (lambda ()
                               (define-clojure-indent
                                 ;; re-frame
                                 (reg-cofx :defn)
                                 (reg-event-db :defn)
                                 (reg-event-fx :defn)
                                 (reg-fx :defn)
                                 (reg-sub :defn)
                                 (reg-event-domain :defn)
                                 (reg-block-event-fx :defn)
                                 (reg-event-domain-fx :defn))
                               (modify-syntax-entry ?/ "_" clojure-mode-syntax-table)
                               (modify-syntax-entry ?- "w" clojure-mode-syntax-table)
                               (modify-syntax-entry ?? "w" clojure-mode-syntax-table)))

(defun spacemacs/clj-find-var ()
  "Attempts to jump-to-definition of the symbol-at-point. If CIDER fails, or not available, falls back to dumb-jump"
  (interactive)
  (let ((var (cider-symbol-at-point)))
    (if (and (cider-connected-p) (cider-var-info var))
        (unless (eq 'symbol (type-of (cider-find-var nil var)))
          (dumb-jump-go))
      (dumb-jump-go))))

;; stolen from spacemacs
(defun ben/sudo-edit (&optional arg)
  (interactive "P")
  (let ((fname (if (or arg (not buffer-file-name))
                   (read-file-name "File: ")
                 buffer-file-name)))
    (find-file
     (cond ((string-match-p "^/ssh:" fname)
            (with-temp-buffer
              (insert fname)
              (search-backward ":")
              (let ((last-match-end nil)
                    (last-ssh-hostname nil))
                (while (string-match "@\\\([^:|]+\\\)" fname last-match-end)
                  (setq last-ssh-hostname (or (match-string 1 fname)
                                              last-ssh-hostname))
                  (setq last-match-end (match-end 0)))
                (insert (format "|sudo:%s" (or last-ssh-hostname "localhost"))))
              (buffer-string)))
           (t (concat "/sudo:root@localhost:" fname))))))

(display-time-mode t)

(setq calendar-longitude 13.4334646
      calendar-latitude 52.4714836
      display-time-24hr-format t
      alert-default-style 'libnotify)

(setq sql-connection-alist
      '((local-pitch (sql-product 'postgres)
                     (sql-database (concat "postgresql://pitch_super@localhost:5432/pitch?options=--search_path%3d" (getenv "PITCH_STAGE"))))))

(defalias 'eshell/vi #'eshell/emacs)
(defalias 'eshell/vim #'eshell/emacs)
